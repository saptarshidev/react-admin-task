import React from 'react';

import jsonServerProvider from 'ra-data-json-server';
import { Admin, Resource, ListGuesser } from 'react-admin';
import { UserList } from './Users';

const dataProvider = jsonServerProvider('https://jsonplaceholder.typicode.com');


const Main = () => {
  return (
    <React.Fragment>
      <Admin dataProvider={dataProvider}>
        <Resource name="users" list={ListGuesser} />
        <Resource name="users" list={UserList} />
      </Admin>
    </React.Fragment>
  );
}


export default Main;